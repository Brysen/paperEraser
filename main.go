package main

import (
	"flag"
	"fmt"
	"image"
	"log"
	"math"
	"os"
	"strings"
	"time"

	"image/color"
	"image/draw"
	_ "image/jpeg"
	"image/png"
)

func main() {
	var (
		invertBlackPixels bool
		timeProcessing    bool
		outputfilePath    string
	)
	flag.BoolVar(&invertBlackPixels, "i", false, "invert blackish pixels. Meant to invert black pens")
	flag.BoolVar(&timeProcessing, "t", false, "time how long it takes to process the image")
	flag.StringVar(&outputfilePath, "f", "output.png", "For setting an output path. Prints to STDOUT if omitted")
	flag.Parse()

	outputfilePath = strings.TrimSpace(outputfilePath)

	file, err := os.Open(flag.Arg(0))
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	img, _, err := image.Decode(file)
	if err != nil {
		log.Fatal(err)
	}

	// Using NRGBA because pre alpha multiplied colors can be a bitch
	// if you don't know about them
	imgSrc := AsShallowNRGBA(img)
	imgOut := image.NewNRGBA(img.Bounds())

	var width int = img.Bounds().Max.X
	var height int = img.Bounds().Max.Y

	if timeProcessing {
		var startProcessingTime time.Time = time.Now()

		processImage(width, height, imgSrc, imgOut, invertBlackPixels)

		fmt.Println("Image processed in:", time.Since(startProcessingTime))
	} else {
		processImage(width, height, imgSrc, imgOut, invertBlackPixels)
	}

	outputFile, err := os.Create(outputfilePath)
	if err != nil {
		log.Fatal(err)
	}
	defer outputFile.Close()
	png.Encode(outputFile, imgOut)
}

func processImage(width int, height int, imgSrc *image.NRGBA, imgOut *image.NRGBA, invertBlackPixels bool) {
	for x := 0; x < width; x++ {
		go processImageLine(height, x, imgSrc, imgOut, invertBlackPixels)
	}
}

func processImageLine(height, x int, imgSrc *image.NRGBA, imgOut *image.NRGBA, invertBlackPixels bool) {
	for y := 0; y < height; y++ {
		rgba := imgSrc.NRGBAAt(x, y)
		hsl := RGBAToHSL(rgba)

		if hsl.S > 0.1 && inRange(hsl.L, 0.2, 0.7) {

			imgOut.SetNRGBA(x, y, rgba)
		}
		if invertBlackPixels && (hsl.S < 0.25 && hsl.L < 0.4) {

			imgOut.SetNRGBA(x, y, color.NRGBA{R: 255 - rgba.R, G: 255 - rgba.G, B: 255 - rgba.B, A: rgba.A})
		}
	}
}

func inRange(input, min, max float64) bool {
	return input >= min && input <= max
}

// AsNRGBA returns an RGBA copy of the supplied image.
func AsNRGBA(src image.Image) *image.NRGBA {
	bounds := src.Bounds()
	img := image.NewNRGBA(bounds)
	draw.Draw(img, bounds, src, bounds.Min, draw.Src)
	return img
}

// AsShallowNRGBA tries to cast to image.RGBA to get reference. Otherwise makes a copy
func AsShallowNRGBA(src image.Image) *image.NRGBA {
	if rgba, ok := src.(*image.NRGBA); ok {
		return rgba
	}
	return AsNRGBA(src)
}

type HSL struct {
	H, S, L float64
}

func RGBAToHSL(rgba color.NRGBA) HSL {
	var h, s, l float64
	r := float64(rgba.R) / 255.0
	g := float64(rgba.G) / 255.0
	b := float64(rgba.B) / 255.0

	cmax := math.Max(r, math.Max(g, b))
	cmin := math.Min(r, math.Min(g, b))
	delta := cmax - cmin

	// Calculate Hue
	if delta == 0 {
		h = 0
	} else if cmax == r {
		h = math.Mod((g-b)/delta, 6.0)
	} else if cmax == g {
		h = ((b-r)/delta + 2.0)
	} else {
		h = ((r-g)/delta + 4.0)
	}
	h *= 60.0
	if h < 0 {
		h += 360.0
	}

	// Calculate Lightness
	l = (cmax + cmin) / 2.0

	// Calculate Saturation
	if delta == 0 {
		s = 0
	} else {
		if l < 0.5 {
			s = delta / (2.0 * l)
		} else {
			s = delta / (2.0 - 2.0*l)
		}
	}

	return HSL{H: h, S: s, L: l}
}
