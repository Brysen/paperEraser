# Paper Eraser

Just scanned your document and would rather have a transparent background rather than white? Paper Eraser is for you.

## Installation

Install with `go install`
```bash
go install "gitlab.com/Brysen/paperEraser@latest"
```

## Usage

To erase the white background of an image, simply run the command with an image as an argument.
```bash
paperEraser path/to/input/image.png|jpg
```

To invert any black pixels (black pen), run with the -i flag
```bash
paperEraser -i path/to/input/image.png|jpg
```

To specifify an output path, run with the -f flag and path to write the output to.
Defaults to `output.png` in the current working directory if omitted. 
```bash
paperEraser -f path/to/Output/image.png path/to/input/image.png|jpg
```

With the `-t` flag, the program will print out the time it took to process the image.
```bash
$ paperEraser.exe -i -t -f path/to/Output/image.png path/to/input/image.png|jpg
Image processed in: 8.0024ms
```